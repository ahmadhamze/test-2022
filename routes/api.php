<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\InventoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('add_user',[UserController::class,'create']);
Route::get('get_user/{id}',[UserController::class,'show']);
Route::post('add_unit',[UnitController::class,'create']);
Route::post('add_product',[ProductController::class,'create']);
Route::get('get_product/{product_id}/{unit_id?}',[ProductController::class,'show']);


