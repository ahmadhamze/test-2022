<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'unit_id',
        'amount',
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        return $this->belongsTo(Unit::class,'unit_id','id');

    }

    public function getTotalQuantityAttribute()
    {
        return $this->units->modifier * $this->amount;
    }

    public function getImagePathAttribute()
    {
        $img=Image::where('o_id',$this->id)->first();
        return $img->path;
    }

}
