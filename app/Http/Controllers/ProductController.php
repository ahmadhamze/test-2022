<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'unit_id' => 'required|numeric',
                'amount' => 'required|numeric',
        ]);
        if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
        }
        $photo=null;
        if ($request->hasFile('photo')) {
            $photo = time() . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(public_path() . '/img/product/', $photo);
        }
        $product = new Product();
        $product->name=$request->name;
        $product->unit_id=$request->unit_id;
        $product->amount=$request->amount;
        $product->save();
        $image = new Image();
        $image->o_id=$product->id;
        $image->o_type='product';
        $image->description=$request->description;
        $image->path=asset('img/product/'.$photo);
        $image->save();
        return response()->json(['Message' => 'Successfully Add Product']);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show($product_id,$unit_id=null)
    {
        $product =Product::find($product_id);
        if($unit_id){
        return response()->json(['total_quantity_by_unit_id' => $product]);
        }
        return response()->json(['total_quantity' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
